module RefalAst where

type Program = [Declaration]

data Declaration = Declaration String Block
    deriving Show

type Block = [Sentence]

data Sentence = Sentence Condition Substitution
    deriving Show

data Condition = Condition (Expression Unbound) [AdditionalCondition]
    deriving Show

data AdditionalCondition = Is (Expression Evaluable) (Expression Unbound)
    deriving Show

data Substitution = ExpressionSubstitution (Expression Evaluable)
                  | BlockApplicationSubstitution Block (Expression Evaluable)
                  deriving Show

type Expression a = [Term a]

data Term a = Term a
            | Structure (Expression a)
            deriving Show

data Unbound = Unbound Symbol
             | UnboundVariable Type String
             deriving Show

data Evaluable = Evaluable Unbound
               | FunctionApplication String (Expression Evaluable)
               deriving Show

data Symbol = Character Char
            | Identifier String
            | Number Integer
            | RealNumber Double
            deriving Show

data Type = SymbolType | TermType | ExpressionType
    deriving Show
