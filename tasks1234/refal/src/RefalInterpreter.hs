module RefalInterpreter where

import RefalAst

--type Function = Scope -> Expression Symbol -> Expression Symbol
--
--data StoredValue = StoredFunction Function
--    | StoredExpression (Expression Symbol)
--    | StoredTerm (Term Symbol)
--    | StoredSymbol Symbol
--
--data Scope = Scope [(String, StoredValue)]
--
--withSomething :: String -> StoredValue -> Scope -> Scope
--withSomething name sv (Scope svs) = Scope $ (name, sv) : svs
--
--get :: Scope -> String -> StoredValue
--get (Scope svs) name = return value where
--    value = lookup name svs
--    return Nothing = undefined
--    return (Just sv) = sv
--
--initialized :: Scope
--initialized = undefined -- add built-in functions
--
--functionFromBlock :: Block -> Scope -> Expression Symbol -> Expression Symbol
--functionFromBlock block = undefined
--
--class Evaluable a where
--    eval :: a -> Scope -> Expression Symbol
--
--concatExpressions :: Expression a -> Expression a -> Expression a
--concatExpressions (Expression e1) (Expression e2) = Expression (e1 ++ e2)
--
--instance Evaluable Program where
--    eval (Program []) = eval $ Call "Go" $ Expression []
--    eval (Program ((Declaration name block) : decls)) = eval (Program decls) . addFun where
--        addFun = withSomething name $ StoredFunction $ functionFromBlock block
--
--instance Evaluable a => Evaluable (Expression a) where
--    eval (Expression (term : terms)) scope = concatExpressions (eval term scope) (eval (Expression terms) scope)
--
--instance Evaluable a => Evaluable (Term a) where
--    eval (Term term) scope = eval term scope
--    eval (Structure expr) scope = Expression [Structure $ eval expr scope]
--
--instance Evaluable Common where
--    eval = undefined
--
--instance Evaluable Pattern where
--    eval = undefined
--
--instance Evaluable General where
--    eval = undefined
