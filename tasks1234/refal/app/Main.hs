module Main where

import RefalAst
import RefalParser

import Text.Megaparsec
import Text.Pretty.Simple (pPrint)

main :: IO ()
main = do
    p <- runParser program "app/pal" <$> readFile "app/pal"
    showr p where
        showr (Left e) = putStrLn $ show e
        showr (Right p) = pPrint p
