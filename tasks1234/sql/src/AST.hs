module AST where

import Prelude hiding (Ordering)
import Data.Text (Text)
import Data.Typeable
import Data.Int
import Data.List.NonEmpty

data UInt
  = UInt Int
  deriving Show

data Type
  = VarChar UInt
  | Int
  deriving Show

data Literal
  = StringLiteral String
  | NumLiteral Int
  | Null
  deriving Show

data DefinedName
  = DefinedName String
  deriving Show

data ColumnId
  = ColumnId DefinedName
  deriving Show

data ColumnName
  = ColumnName (Maybe TableName) ColumnId
  deriving Show

data Statement 
  = CreateTable Create
  | SelectSmth  Select
  | InsertSmth  Insert
  | UpdateSmth  Update
  | RemoveSmth  Remove
  deriving Show

data StatementList
  = StatementList (NonEmpty Statement)
  deriving Show

data ColumnAndType = ColumnAndType ColumnId Type
  deriving Show

data Create 
  = Create TableName (NonEmpty ColumnAndType)
  deriving Show

data TableId
  = TableId DefinedName
  deriving Show

data TableName
  = TableName TableId
  deriving Show

data SelectSubList
  = SelectSubList Expr
  deriving Show

data Select
  = Select 
  { selectList :: SelectList
  , selectedTablesList :: SelectedTablesList
  , selectWhere :: Maybe SearchCondition }
  deriving Show

data SelectList
  = SelectAll
  | SelectList (NonEmpty SelectSubList)
  deriving Show

type ColumnIdList = NonEmpty ColumnId

data Insert
  = Insert TableName (Maybe ColumnIdList) InsertValueList
  deriving Show

data InsertValue
  = InsertInt Expr
  | InsertVarchar String
  | InsertNull
  deriving Show

type InsertValueList = NonEmpty InsertValue

data UpdateValue
  = UpdateValueExpr Expr
  | UpdateNull
  deriving Show

data UpdateColumn
  = UpdateColumn ColumnId UpdateValue
  deriving Show

type UpdateColumnList = NonEmpty UpdateColumn

data Update
  = Update
  { updateTable   :: TableName
  , updateColumns :: UpdateColumnList
  , updateWhere   :: Maybe SearchCondition }
  deriving Show

data Remove
  = Remove
  { removeTable :: TableName
  , removeWhere :: Maybe SearchCondition }
  deriving Show  

data Ordering
  = Asc
  | Desc 
  deriving Show

data Limit
  = Limit UInt
  deriving Show

data GroupBy
  = GroupBy [ Expr ]
  deriving Show

data Expr
  = ColumnNameLiteral String
  | LiteralValue Literal
  | Negation Expr
  | Sum      Expr Expr
  | Subtr    Expr Expr
  | Product  Expr Expr
  | Division Expr Expr
  | LE       Expr Expr
  | GE       Expr Expr
  | Less     Expr Expr
  | Gr       Expr Expr
  | Eq       Expr Expr
  | Not      Expr 
  | NotEq    Expr Expr
  | And      Expr Expr
  | Or       Expr Expr
  deriving Show

data SelectedTable
  = SelectedTable TableName
  deriving Show
 
data SelectedTablesList 
  = SelectedTablesList (NonEmpty SelectedTable)
  deriving Show

data SearchCondition
  = SearchCondition Expr
  deriving Show