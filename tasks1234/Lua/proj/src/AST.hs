module AST where

data Literal
    = Var    Literal
    | Call   Statement 
    | AConst Double
    | BConst Bool
    | SConst String
    | Nil
    | BiOp   Op Literal Literal
    | UnOp   Op Literal
    deriving (Show)

data Op
    = Add | Sub | Mul | Div 
    | Mod | Pow | Not | And 
    | Or  | Eq  | Lt  | Le  | Fld
    | Len | Ne  | Gt  | Ge  | Con
    deriving (Show)

data Statement
    = Assign     [Literal] [(Either Literal Statement)]
    | Define     Literal [Literal] Statement
    | Function   Literal [Literal]
    | Block      [Statement]
    | IfThenElse Literal Statement (Maybe Statement)
    | For        Literal Literal Literal Literal Statement
    | ForEach    [Literal] [Literal] Statement
    | While      Literal Statement
    | Repeat     Statement Literal
    | Local      Statement
    | Return     Literal
    | Label      Literal
    | Goto       Literal
    | Break
    deriving (Show)
