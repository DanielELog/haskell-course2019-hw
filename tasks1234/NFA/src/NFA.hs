module NFA where

data Label = Label {name :: String, value :: String} deriving Show

data State = State Char | LState Char [Label] deriving Show

data Transition = Transition State State | LTransition State State [Label] deriving Show

data Word = Word [Transition] deriving Show

data InitialStates = InitialStates [State] deriving Show

data Registers = Registers [Int] | LRegisters [(Int, [Label])] deriving Show

{-
Examle of automaton

{
a;
b;
c [color = green];
a -> c [style = bold];
a -> b -> c;
IS a;
}
-}